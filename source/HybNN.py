import numpy as np
import torch
import torch.nn as nn
import torch.nn.functional as F


class HybNN(nn.Module):
    def __init__(self, n_chan, k):
        super(HybNN, self).__init__()
        self.n_chan = n_chan
        self.layer1 = nn.Linear(75, 6 * n_chan)
        self.dropout_feature = nn.Dropout(p = 0.5)
        self.dropout_adj = nn.Dropout(p = 0.5)
        self.dropout_final = nn.Dropout(p = 0.5)
        self.k = k
        self.conv1d_1 = nn.Conv1d(in_channels = 4 * n_chan, out_channels = 5 * n_chan // 2, kernel_size = (self.k + 1) // 2 + 1)
        self.conv1d_2 = nn.Conv1d(in_channels = 5 * n_chan // 2, out_channels= n_chan, kernel_size = self.k // 2 + 1)
        self.batch_norm = nn.BatchNorm1d(n_chan)
        self.conv1d_3 = nn.Conv1d(in_channels = 5 * n_chan, out_channels = 6 * n_chan // 2, kernel_size = (self.k + 1) // 2 + 1)
        self.conv1d_4 = nn.Conv1d(in_channels = 6 * n_chan // 2, out_channels = n_chan, kernel_size = self.k // 2 + 1)
        self.conv1d_5 = nn.Conv1d(in_channels = 6 * n_chan, out_channels = 7 * n_chan // 2, kernel_size = (self.k + 1) // 2 + 1)
        self.conv1d_6 = nn.Conv1d(in_channels = 7 * n_chan // 2, out_channels = n_chan, kernel_size = self.k // 2 + 1)
        self.conv1d_7 = nn.Conv1d(in_channels = 7 * n_chan, out_channels = 8 * n_chan // 2, kernel_size = (self.k + 1) // 2 + 1)
        self.conv1d_8 = nn.Conv1d(in_channels = 8 * n_chan // 2, out_channels = n_chan, kernel_size = self.k // 2 + 1)
        self.conv1d_9 = nn.Conv1d(in_channels = 8 * n_chan, out_channels = 9 * n_chan // 2, kernel_size = (self.k + 1) // 2 + 1)
        self.conv1d_10 = nn.Conv1d(in_channels = 9 * n_chan // 2, out_channels = n_chan, kernel_size = self.k // 2 + 1)
        self.conv1d_11 = nn.Conv1d(in_channels = 9 * n_chan, out_channels = 10 * n_chan // 2, kernel_size = (self.k + 1) // 2 + 1)
        self.conv1d_12 = nn.Conv1d(in_channels = 10 * n_chan // 2, out_channels = n_chan, kernel_size = self.k // 2 + 1)
        self.conv1d_13 = nn.Conv1d(in_channels = 9 * n_chan, out_channels = 10 * n_chan // 2, kernel_size = (self.k + 1) // 2 + 1)
        self.conv1d_14 = nn.Conv1d(in_channels = 10 * n_chan // 2, out_channels = n_chan, kernel_size = self.k // 2 + 1)
        self.relu = nn.ReLU()
        self.layer2 = nn.Linear(6 * n_chan, n_chan)
        self.predict_property = nn.Linear(n_chan + 363, 1)
        self.conv1d = nn.Conv1d(batch * 16, self.k, 3, stride = 1, padding = 1)
        self.lnn1 = nn.Linear(363, 512)
        self.lnn2 = nn.Linear(512, 1024)
        self.lnn3 = nn.Linear(1024, 363)

    def pad(self, matrices, value):
        sizes = [d.shape[0] for d in matrices]
        dim = sum(sizes)
        pad_matrices = value + np.zeros((dim, dim))
        m = 0
        for i, d in enumerate(matrices):
            s_i = sizes[i]
            pad_matrices[m:m + s_i, m:m + s_i] = d.cpu()
            m += s_i
        return torch.FloatTensor(pad_matrices).to(device)

    def sum_axis(self, xs, axis):
        y = list(map(lambda x: torch.sum(x, 0), torch.split(xs, axis)))
        return torch.stack(y)

    def convolution_fun1(self, adjacency, features):
        adj_m = self.dropout_adj(adjacency)
        out = self.dropout_feature(features)
        out = self.layer1(out)
        out = torch.matmul(adj_m, out)
        return out

    def join(self, adj_m, feat):
        adj_m_graph1 = torch.unsqueeze(adj_m, 1)
        feat_graph1 = torch.unsqueeze(feat, -1)
        feas = torch.mul(adj_m_graph1, feat_graph1)
        feas = feas.permute(2, 1, 0)
        features = feas.permute(0, 2, 1)

        spatial_feature = self.conv1d(features)
        spatial_feature = spatial_feature.permute(0, 2, 1)
        out = torch.cat([feat_graph1, spatial_feature], 2)
        out = out.permute(0, 2, 1)
        return out

    def SGRU1(self, adjacency, feat):
        adj_m = self.dropout_adj(adjacency)
        out = self.join(adj_m, feat)
        out = self.dropout_feature(out)
        out = out.permute(0, 2, 1)
        ret_conv1d_1 = self.conv1d_1(out)
        ret_conv1d_1_relu = F.relu(ret_conv1d_1)
        ret_conv1d_1_relu_drought = self.dropout_feature(ret_conv1d_1_relu)
        ret_conv1d_2 = self.conv1d_2(ret_conv1d_1_relu_drought)
        ret_conv1d_2_relu = F.relu(ret_conv1d_2)
        ret_conv1d_2_permute = ret_conv1d_2_relu.permute(0, 2, 1)
        ret_conv1d_2_permute_unbind = torch.unbind(ret_conv1d_2_permute, dim = 1)
        ret_conv1d_2_batch_norm = self.batch_norm(ret_conv1d_2_permute_unbind[0])
        return ret_conv1d_2_batch_norm

    def SGRU2(self, adjacency, feat):
        adj_m = self.dropout_adj(adjacency)
        out = self.join(adj_m, feat)
        out = self.dropout_feature(out)
        out = out.permute(0, 2, 1)
        ret_conv1d_3 = self.conv1d_3(out)
        ret_conv1d_3_relu = F.relu(ret_conv1d_3)
        ret_conv1d_3_relu_drought = self.dropout_feature(ret_conv1d_3_relu)

        ret_conv1d_4 = self.conv1d_4(ret_conv1d_3_relu_drought)
        ret_conv1d_4 = F.relu(ret_conv1d_4)
        ret_conv1d_4_permute = ret_conv1d_4.permute(0, 2, 1)
        ret_conv1d_4_permute_unbind = torch.unbind(ret_conv1d_4_permute, dim = 1)
        ret_conv1d_4_batch_norm = self.batch_norm(ret_conv1d_4_permute_unbind[0])
        return ret_conv1d_4_batch_norm

    def SGRU3(self, adjacency, feat):
        adj_m = self.dropout_adj(adjacency)
        out = self.join(adj_m, feat)
        out = self.dropout_feature(out)
        out = out.permute(0, 2, 1)
        ret_conv1d_5 = self.conv1d_5(out)
        ret_conv1d_5_relu = F.relu(ret_conv1d_5)
        ret_conv1d_5_relu_drought = self.dropout_feature(ret_conv1d_5_relu)
        ret_conv1d_6 = self.conv1d_6(ret_conv1d_5_relu_drought)
        ret_conv1d_6 = F.relu(ret_conv1d_6)
        ret_conv1d_6_permute = ret_conv1d_6.permute(0, 2, 1)
        ret_conv1d_6_permute_unbind = torch.unbind(ret_conv1d_6_permute, dim = 1)
        ret_conv1d_6_batch_norm = self.batch_norm(ret_conv1d_6_permute_unbind[0])
        return ret_conv1d_6_batch_norm

    def SGRU4(self, adjacency, feat):
        adj_m = self.dropout_adj(adjacency)
        out = self.join(adj_m, feat)
        out = self.dropout_feature(out)
        out = out.permute(0, 2, 1)
        ret_conv1d_7 = self.conv1d_7(out)
        ret_conv1d_7_relu = F.relu(ret_conv1d_7)
        ret_conv1d_7_relu_drought = self.dropout_feature(ret_conv1d_7_relu)
        ret_conv1d_8 = self.conv1d_8(ret_conv1d_7_relu_drought)
        ret_conv1d_8 = F.relu(ret_conv1d_8)
        ret_conv1d_8_permute = ret_conv1d_8.permute(0, 2, 1)
        ret_conv1d_8_permute_unbind = torch.unbind(ret_conv1d_8_permute, dim = 1)
        ret_conv1d_8_batch_norm = self.batch_norm(ret_conv1d_8_permute_unbind[0])
        return ret_conv1d_8_batch_norm

    def SGRU5(self, adjacency, feat):
        adj_m = self.dropout_adj(adjacency)
        out = self.join(adj_m, feat)
        out = self.dropout_feature(out)
        out = out.permute(0, 2, 1)
        ret_conv1d_9 = self.conv1d_9(out)
        ret_conv1d_9_relu = F.relu(ret_conv1d_9)
        ret_conv1d_9_relu_drought = self.dropout_feature(ret_conv1d_9_relu)
        ret_conv1d_10 = self.conv1d_10(ret_conv1d_9_relu_drought)
        ret_conv1d_10 = F.relu(ret_conv1d_10)
        ret_conv1d_10_permute = ret_conv1d_10.permute(0, 2, 1)
        ret_conv1d_10_permute_unbind = torch.unbind(ret_conv1d_10_permute, dim = 1)
        ret_conv1d_10_batch_norm = self.batch_norm(ret_conv1d_10_permute_unbind[0])
        return ret_conv1d_10_batch_norm

    def SGRU6(self, adjacency, feat):
        adj_m = self.dropout_adj(adjacency)
        out = self.join(adj_m, feat)
        out = self.dropout_feature(out)
        out = out.permute(0, 2, 1)
        ret_conv1d_11 = self.conv1d_11(out)
        ret_conv1d_11_relu = F.relu(ret_conv1d_11)
        ret_conv1d_11_relu_drought = self.dropout_feature(ret_conv1d_11_relu)
        ret_conv1d_12 = self.conv1d_12(ret_conv1d_11_relu_drought)
        ret_conv1d_12 = F.relu(ret_conv1d_12)
        ret_conv1d_12_permute = ret_conv1d_12.permute(0, 2, 1)
        ret_conv1d_12_permute_unbind = torch.unbind(ret_conv1d_12_permute, dim = 1)
        ret_conv1d_12_batch_norm = self.batch_norm(ret_conv1d_12_permute_unbind[0])
        return ret_conv1d_12_batch_norm
        
    def SGRU7(self, adjacency, feat):
        adj_m = self.dropout_adj(adjacency)
        out = self.join(adj_m, feat)
        out = self.dropout_feature(out)
        out = out.permute(0, 2, 1)
        ret_conv1d_13 = self.conv1d_13(out)
        ret_conv1d_13_relu = F.relu(ret_conv1d_13)
        ret_conv1d_13_relu_drought = self.dropout_feature(ret_conv1d_13_relu)
        ret_conv1d_14 = self.conv1d_14(ret_conv1d_13_relu_drought)
        ret_conv1d_14 = F.relu(ret_conv1d_14)
        ret_conv1d_14_permute = ret_conv1d_14.permute(0, 2, 1)
        ret_conv1d_14_permute_unbind = torch.unbind(ret_conv1d_14_permute, dim = 1)
        ret_conv1d_14_batch_norm = self.batch_norm(ret_conv1d_14_permute_unbind[0])
        return ret_conv1d_14_batch_norm

    def convolution_fun2(self, adjacency, features):
        adj_m = self.dropout_adj(adjacency)
        out = self.dropout_feature(features)
        out = self.layer2(out)
        out = torch.matmul(adj_m, out)
        return out
        
    def rnn(self, xs):
        xs = torch.unsqueeze(xs, 0)
        xs, h = self.W_rnn(xs)
        xs = torch.relu(xs)
        xs = torch.squeeze(torch.squeeze(xs, 0), 0)
        return torch.unsqueeze(torch.mean(xs, 0), 0)

    def forward(self, entry):
        features, adjacency, smiles = list(entry[0]), list(entry[1]), list(entry[2])
        axis = list(map(lambda x: len(x), features))
        features = torch.cat(features)
        adjacency = self.pad(adjacency, 0)
        out1 = self.convolution_fun1(adjacency, features)
        cur_out1 = self.SGRU1(adjacency, out1)
        out2 = torch.cat((out1, cur_out1), 1)
        cur_out2 = self.SGRU2(adjacency, out2)
        out3 = torch.cat((out2, cur_out2), 1)
        out = self.convolution_fun2(adjacency, out3)
        calc_out = self.sum_axis(out, axis)
        smile_vectors = self.smile_representation(smiles)
        after_smile_vectors = self.rnn(smile_vectors)
        calc_conc = torch.cat((calc_out, after_smile_vectors), 1)
        out_val = self.lnn1(x_con_desc)
        out_val = self.relu(out_val)
        out_val = self.dropout(out_val)
        out_val = self.lnn1(out_val)
        out_val = self.relu(out_val)
        out_val = self.dropout(out_val)
        out_val = self.lnn3(out_val)
        out_val = self.relu(out_val)
        out_val = self.dropout(out_val)
        return self.predict_property(out_val)

    def __call__(self, batch, std, mean, train=True):
        entry, assoc = batch[:-1], torch.squeeze(batch[-1])
        comp_assoc = self.forward(entry)

        if train:
            assoc = torch.unsqueeze(assoc, 1)
            loss = F.mse_loss(comp_assoc, assoc)
            return loss

        else:
            assoc = torch.unsqueeze(assoc, 1)
            loss = F.mse_loss(comp_assoc, assoc)
            z = comp_assoc.to('cpu').data.numpy()
            t = assoc.to('cpu').data.numpy()
            z, t = std * z + mean, std * t + mean
            return loss, z, t
