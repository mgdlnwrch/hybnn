import torch
from utils.operations import set_data


class ChemDataset(torch.utils.data.Dataset):

    def __init__(self, dataset_type):
        with open('../../data/' + 'smile_n_gram.in', 'r') as f:
            smile = f.read().strip().split('\n')
        smile_, smi_word_index, smi_embedding_matrix = smile_to_word(smile, 100, 100)
        self.features = set_data('../../data/' + 'features_info/' + dataset_type, torch.FloatTensor)
        self.adjacency = set_data('../../data/' + 'adjacency_info/' + dataset_type, torch.FloatTensor)
        self.smile_representation = nn.Embedding(100, 100)
        self.smile_representation.weight = nn.Parameter(torch.tensor(smi_embedding_matrix, dtype=torch.float32))
        self.smile_representation.weight.requires_grad = True
        self.assoc = set_data('../../data/' + 'assoc_info/' + dataset_type, torch.FloatTensor)
        self.data = list(zip(np.array(self.features), np.array(self.adjacency), np.array(self.assoc)))
        self.W_rnn = nn.GRU(bidirectional = True, num_layers = 2, input_size = 100, hidden_size = 100)


    def __getitem__(self, ind):
        return self.data[ind]
        

    def __len__(self):
        return len(self.assoc)
