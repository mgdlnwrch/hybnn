import deepchem as dc
from rdkit import Chem
import pandas as pd
import torch
from numpy import flip
import os
import numpy as np
import scipy.sparse as sp


def adjacency_matrix_normalize(adjacency_matrix):
    row = []
    for i in range(adjacency_matrix.shape[0]):
        sum = adjacency_matrix[i].sum()
        row.append(sum)
    rowsum = np.array(row)
    tmp1 = np.power(rowsum, -0.5).flatten()
    tmp1[np.isinf(tmp1)] = 0
    diag_mat = sp.diags(tmp1)
    return diag_mat.dot(adjacency_matrix)


def adjacency_matrix_preprocess(adjacency_matrix, norm = True, is_sparse = False):
    adjacency_matrix = adjacency_matrix + np.eye(len(adjacency_matrix))
    if norm:
        adjacency_matrix = adjacency_matrix_normalize(adjacency_matrix)
    return adjacency_matrix
    

def load_sdf_files(input_files, clean_mols=True):
    dataframes = []
    for input_file in input_files:
        print("Reading from %s." % input_file)
        sDMolSupplier = Chem.SDMolSupplier(str(input_file), clean_mols, False, False)
        df_rows = []
        for index, mol in enumerate(sDMolSupplier):
            if mol is not None:
                smiles = Chem.MolToSmiles(mol)
                df_rows.append([index, smiles, mol])
        mol_df = pd.DataFrame(df_rows, columns=('mol_id', 'smiles', 'mol'))
        dataframes.append(mol_df)
  return dataframes
  

def mol_to_adjacency_matrix(mol):
    adjacency_matrix = Chem.GetAdjacencyMatrix(mol)
    return np.array(adjacency_matrix,dtype=float)
  
  
def align_features(feature_array, adjacency_matrix, atoms_no=15):
    attributes = np.zeros((atoms_no, 70))
    if len(feature_array) <= atoms_no:
        attributes[0:len(feature_array), 0:70] = feature_array
    else:
        attributes = feature_array[0:atoms_no]
    adjacency = np.zeros((atoms_no, atoms_no))
    if len(feature_array) <= atoms_no:
        adjacency[0:len(feature_array), 0:len(feature_array)] = adjacency_matrix
    else:
        adjacency = adjacency_matrix[0:atoms_no, 0:atoms_no]
    return attributes, adjacency


def get_feature(dataset, activity_label=1):
    features_info, adjacency_info, edge_info, final_feature = [], [], [], []
    assoc_list, smiles_list = [], []

    for index, row in dataset.iterrows():
        smiles_list.append(row["smiles"])
        assoc_list.append(activity_label)
        mol = Chem.MolFromSmiles(row["smiles"])
        if not mol:
            raise ValueError("SMILES parsing error:", row["smiles"])
        featurizer = dc.feat.ConvMolFeaturizer()
        featurization_effect = featurizer.featurize([mol])
        x = featurization_effect[0]
        mol_atom_features = x.get_atom_features()
        mol_adjacency_matrix = mol_to_adjacency_matrix(mol)
        mol_atom_features_final = flip(mol_atom_features, 0)
        mol_adjacency_matrix_final = flip(mol_adjacency_matrix, 0)
        mol_atom_features_aligned, mol_adjacency_matrix_aligned = align_features(mol_atom_features_final, mol_adjacency_matrix_final)
        features_info.append(np.array(mol_atom_features_aligned))
        djacency_matrix_preprocessed = adjacency_matrix_preprocess(mol_adjacency_matrix_aligned)
        adjacency_info.append(djacency_matrix_preprocessed)
        ind = np.array(np.where(mol_adjacency_matrix_final == 1))
        edge_ind = torch.from_numpy(ind).long()
        edge_info.append(edge_ind)
        atom_features_tmp = torch.from_numpy(mol_atom_features_final.copy()).float()
        final_feature.append(atom_features_tmp)

    return features_info, adjacency_info, edge_info, final_feature, assoc_list, smiles_list


def save(directory_, features, adjacency, assoc, smiles_list, edge_info, final_feature, dataset_type = None):
    np.save(directory_ + '/features_info/' + dataset_type + '/', features)
    np.save(directory_ + '/adjacency_info/' + dataset_type + '/', adjacency)
    np.save(directory_ + '/assoc_info/' + dataset_type + '/', assoc)
    np.save(directory_ + '/smiles/' + dataset_type + '/', smiles_list)

    with open(directory_ + '/edge_info/' + dataset_type + '/', 'wb') as f:
        pickle.dump(edge_info, f)

    with open(directory_ + '/final_feature/' + dataset_type + '/', 'wb') as a:
        pickle.dump(final_feature, a)


directory = '../../data/'
dataset = ['data/AID_1915.sdf']

dataset_df = load_sdf_files(dataset)
for df in dataset_df:
    print(df.head())
    features_info, adjacency_info, edge_info, final_feature, assoc_list, smiles_list =  get_feature(df)
    
save(directory, features_info, adjacency_info, edge_info, final_feature, assoc_list, smiles_list, dataset_type = 'training')
