import numpy as np
from sklearn.metrics import mean_squared_error
from keras.preprocessing import text, sequence


def smile_to_word(smile, maxlength, vector_size):
    tokenizer = text.Tokenizer(num_words = 100, lower = False, filters = "　")
    tokenizer.fit_on_texts(smile)
    smile_seq = sequence.pad_sequences(tokenizer.texts_to_sequences(smile), maxlen = maxlength)

    word_index = tokenizer.word_index
    nb_words = len(word_index)
    print(nb_words)
    vec = {}
    with open("../../data/embeddings/Atom.vec", encoding = 'utf8') as f:
        for line in f:
            tmp = line.rstrip().rsplit(' ')
            word = tmp[0]
            coefs = np.asarray(tmp[1:], dtype='float32')
            vec[word] = coefs
    count = 0
    embedding_matrix = np.zeros((nb_words + 1, vector_size))
    for word, i in word_index.items():
        vector_representation = vec[word] if word in vec else None
        if vector_representation is not None:
            count += 1
            embedding_matrix[i] = vector_representation
        else:
            unk_vec = np.random.random(vector_size) * 0.5
            unk_vec = unk_vec - unk_vec.mean()
            embedding_matrix[i] = unk_vec
    del vec
    return smile_seq, word_index, embedding_matrix


def rms_score(y_true, y_pred):
    return np.sqrt(mean_squared_error(y_true, y_pred))


def set_data(file_name, dtype):
    return [dtype(d).to(device) for d in np.load(file_name + '.npy')]


def metric(RMSE_k_test):
    RMSE_mean_test = np.mean(np.array(RMSE_k_test))
    RMSE_std_test = np.std(np.array(RMSE_k_test))
    return RMSE_mean_test, RMSE_std_test

