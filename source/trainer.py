import torch.optim as optim


class Trainer(object):
    def __init__(self, model, mean, std):
        self.model = model
        self.optimizer = optim.Adam(self.model.parameters(), lr = lr, weight_decay = 5e-3)
        self.std = std
        self.mean = mean

    def train(self, training_loader):
        loss_total = 0
        num = 0
        for data in training_loader:
            num += 1
            self.optimizer.zero_grad()
            loss = self.model(data, mean = self.mean, std = self.std, train = True)
            loss.backward()
            self.optimizer.step()
            loss_total += loss.to('cpu').data.numpy()
        loss_mean = loss_total/num
        return loss_mean
