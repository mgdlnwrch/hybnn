from utils.operations import rms_score


class Tester(object):
    def __init__(self, model, mean, std):
        self.model = model
        self.mean = mean
        self.std = std

    def test(self, test_loader):
        loss_total = 0
        pred = []
        tested = []
        num = 0
        for data in test_loader:
            num += 1
            loss, predicted, true = self.model(data, mean = self.mean, std = self.std, train = False)
            for i in predicted:
                pred.append(float(i))
            for j in true:
                tested.append(float(j))
            loss_total += loss.to('cpu').data.numpy()
        RMSE_val = rms_score(tested, pred)
        loss_mean = loss_total / num
        return loss_mean, RMSE_val, pred, tested
