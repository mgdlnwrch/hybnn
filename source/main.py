import torch
from torch.utils.data import DataLoader
from utils.dataset import ChemDataset
from utils.operations import metric
from HybNN import HybNN
from tester import Tester
from trainer import Trainer
import os


def main():
    os.environ["CUDA_VISIBLE_DEVICES"] = "0"
    device = torch.device('cuda')

    mean = -2.83
    std = 2.87
    iteration = 30
    window = 5
    batch = 8
    k = 4
    n_chan = 4
    decay_interval = 8
    learning_rate = 5e-3
    learning_rate_decay = 0.5
    xlearning_rate, learning_rate_decay = map(float, [learning_rate, learning_rate_decay])
    validation_RMSE = []
    RMSE_test = []

    seed_list = [256, 512, 1024]

    training = ChemDataset('training')
    validation = ChemDataset('validation')
    testing = ChemDataset('testing')
    loaders = [DataLoader(training, batch_size = batch, shuffle = True, drop_last = True),
              DataLoader(validation, batch_size = batch, shuffle = True, drop_last = True),
              DataLoader(testing, batch_size = batch, shuffle = True, drop_last = True)]

    for seed in seed_list:

        loss_train = []
        loss_valid = []
        loss_test = []
        torch.manual_seed(seed)
        torch.cuda.manual_seed_all(seed)
        model = HybNN(n_chan, k).to(device)
        trainer = Trainer(model.train(), mean, std)
        tester = Tester(model.eval(), mean, std)
        for epoch in range(1, (iteration + 1)):
            if epoch  % decay_interval == 0:
                trainer.optimizer.param_groups[0]['learning_rate'] *= learning_rate_decay
            training_loss = trainer.train(loaders[0])
            validation_loss, RMSE_valid, predicted_valid, true_valid = tester.test(loaders[1])
            testing_loss, RMSE_test, predicted_test, true_test = tester.test(loaders[2])
            loss_train.append(training_loss)
            loss_valid.append(validation_loss)
            loss_test.append(testing_loss)
            print(
                'epoch no.: %d-train loss: %.3f, validation loss: %.3f, test loss: %.3f, validation (RMSE): %.3f, test (RMSE): %.3f' %
                (epoch, training_loss, validation_loss, testing_loss, RMSE_valid, RMSE_test))
            if epoch == iteration:
                validation_RMSE.append(RMSE_valid)
                RMSE_test.append(RMSE_test)
        print('validation_RMSE', validation_RMSE)
        print('RMSE_test', RMSE_test)

    RMSE_mean_validation, RMSE_std_validation = metric(validation_RMSE)
    RMSE_mean_testing, RMSE_std_testing = metric(RMSE_test)

    print('out-validation --> mean (RMSE): %.3f, std (RMSE): %.3f' % (RMSE_mean_validation, RMSE_std_validation))
    print('out-testing --> mean (RMSE): %.3f, std (RMSE): %.3f' % (RMSE_mean_testing, RMSE_std_testing))


if __name__ == "__main__":
    main()
