# About HybNN: Hybrid Deep Neural Network

The architecture proposed in this work is the modeling of molecules to predict bioactivity.

# Installation

## Data

Please see the [readme](https://bitbucket.org/mgdlnwrch/hybnn/src/main/toy_ex/) for detailed explanation.

## Requirements

You'll need to install following in order to run the codes.

*  [Python 3.7](https://www.python.org/downloads/)
*  [Keras 2.x]
*  numpy

Please see the [readme](https://bitbucket.org/mgdlnwrch/hybnn/src/main/toy_ex/) for detailed explanation.
